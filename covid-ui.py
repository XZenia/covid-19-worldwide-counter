# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'covid.ui'
#
# Created by: PyQt5 UI code generator 5.9.2
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(449, 217)
        font = QtGui.QFont()
        font.setFamily("DejaVu Sans")
        font.setPointSize(12)
        MainWindow.setFont(font)
        MainWindow.setCursor(QtGui.QCursor(QtCore.Qt.ArrowCursor))
        MainWindow.setStyleSheet("background-color:black;\n"
"color: #00FF00;\n"
"")
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.Counter_Label = QtWidgets.QLabel(self.centralwidget)
        self.Counter_Label.setGeometry(QtCore.QRect(10, 60, 431, 51))
        font = QtGui.QFont()
        font.setFamily("DejaVu Sans")
        font.setPointSize(35)
        self.Counter_Label.setFont(font)
        self.Counter_Label.setAlignment(QtCore.Qt.AlignCenter)
        self.Counter_Label.setWordWrap(True)
        self.Counter_Label.setObjectName("Counter_Label")
        self.Category_Title_Label = QtWidgets.QLabel(self.centralwidget)
        self.Category_Title_Label.setGeometry(QtCore.QRect(20, 20, 411, 23))
        font = QtGui.QFont()
        font.setFamily("DejaVu Sans")
        font.setPointSize(12)
        font.setBold(True)
        font.setWeight(75)
        self.Category_Title_Label.setFont(font)
        self.Category_Title_Label.setScaledContents(True)
        self.Category_Title_Label.setAlignment(QtCore.Qt.AlignCenter)
        self.Category_Title_Label.setObjectName("Category_Title_Label")
        self.Total_Cases_Button = QtWidgets.QPushButton(self.centralwidget)
        self.Total_Cases_Button.setGeometry(QtCore.QRect(20, 140, 111, 51))
        font = QtGui.QFont()
        font.setFamily("DejaVu Sans")
        font.setPointSize(12)
        self.Total_Cases_Button.setFont(font)
        self.Total_Cases_Button.setStyleSheet("background-color:white;\n"
"color:black;")
        self.Total_Cases_Button.setObjectName("Total_Cases_Button")
        self.Recovered_Button = QtWidgets.QPushButton(self.centralwidget)
        self.Recovered_Button.setGeometry(QtCore.QRect(170, 140, 111, 51))
        font = QtGui.QFont()
        font.setFamily("DejaVu Sans")
        font.setPointSize(12)
        self.Recovered_Button.setFont(font)
        self.Recovered_Button.setStyleSheet("background-color:white;\n"
"color:black;")
        self.Recovered_Button.setObjectName("Recovered_Button")
        self.Deaths_Button = QtWidgets.QPushButton(self.centralwidget)
        self.Deaths_Button.setGeometry(QtCore.QRect(320, 140, 111, 51))
        font = QtGui.QFont()
        font.setFamily("DejaVu Sans")
        font.setPointSize(12)
        self.Deaths_Button.setFont(font)
        self.Deaths_Button.setStyleSheet("background-color:white;\n"
"color:black;")
        self.Deaths_Button.setObjectName("Deaths_Button")
        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "COVID-19 Counter"))
        self.Counter_Label.setText(_translate("MainWindow", "5000"))
        self.Category_Title_Label.setText(_translate("MainWindow", "Deaths"))
        self.Total_Cases_Button.setText(_translate("MainWindow", "Total Cases"))
        self.Recovered_Button.setText(_translate("MainWindow", "Recovered"))
        self.Deaths_Button.setText(_translate("MainWindow", "Deaths"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())

